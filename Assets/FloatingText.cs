﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class FloatingText : MonoBehaviour
    {
        public float destroyTime = 3f;
        public Vector3 offset = new Vector3(0, 2, 0);

        void Start()
        {
            Destroy(gameObject, destroyTime);
            gameObject.transform.localPosition += offset;
        }
    }

}
