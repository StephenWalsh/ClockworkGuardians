﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SA
{
    public class EnemyStates : MonoBehaviour
    {
        public int health;
        public GameObject floatingTextPrefab;

        public CharacterStats characterStats;

        public bool isInvincible;
        public bool parryIsOn = true;
        public bool canBeParried = true;
        //public bool doParry = false;
        public bool canMove;
        public bool isDead;
        public bool dontDoAnything;


        public StateManager parriedBy;

        public Animator anim;
        EnemyTarget enTarget;
        AnimatorHook a_hook;
        public Rigidbody rigid;
        public float delta;
        public float poiseDegrade = 2;

        List<Rigidbody> ragdollRigids = new List<Rigidbody>();
        List<Collider> ragdollColliders = new List<Collider>();

        float timer;

        void Start()
        {
            health = 100;
            enTarget = GetComponent<EnemyTarget>();
            anim = GetComponentInChildren<Animator>();
            enTarget.Init(this);

            rigid = GetComponent<Rigidbody>();

            a_hook = anim.GetComponent<AnimatorHook>();
            if (a_hook == null)
                a_hook = anim.gameObject.AddComponent<AnimatorHook>();
            a_hook.Init(null, this);

            InitRagdoll();
            parryIsOn = false;
        }

        void InitRagdoll()
        {
            Rigidbody[] rigs = GetComponentsInChildren<Rigidbody>();
            for (int i = 0; i < rigs.Length; i++)
            {
                if (rigs[i] == rigid)
                    continue;

                ragdollRigids.Add(rigs[i]);
                rigs[i].isKinematic = true;

                Collider col = rigs[i].gameObject.GetComponent<Collider>();
                col.isTrigger = true;
                ragdollColliders.Add(col);
            }
        }

        void EnableRagdoll()
        {
            for (int i = 0; i < ragdollRigids.Count; i++)
            {
                ragdollRigids[i].isKinematic = false;
                ragdollColliders[i].isTrigger = false;
            }
            Collider controllerCollider = rigid.gameObject.GetComponent<Collider>();
            controllerCollider.enabled = false;
            rigid.isKinematic = true;

            StartCoroutine("CloseAnimator");
        }

        IEnumerator CloseAnimator()
        {
            yield return new WaitForEndOfFrame();
            anim.enabled = false;
            this.enabled = false;
        }

        void DisableRagdoll()
        {
            for (int i = 0; i < ragdollRigids.Count; i++)
            {
                ragdollRigids[i].isKinematic = true;
                ragdollColliders[i].isTrigger = true;
            }
        }

        void Update()
        {
            delta = Time.deltaTime;
            canMove = anim.GetBool("can_move");

            if (dontDoAnything)
            {
                dontDoAnything = !canMove;
                return;
            }
                

            if(health <= 0)
            {
                if (!isDead)
                {
                    isDead = true;
                    EnableRagdoll();
                }
            }

            if (isInvincible)
            {
                isInvincible = !canMove;
            }

            if(parriedBy != null && parryIsOn == false)
            {
                parriedBy.parryTarget = null;
                parriedBy = null;
            }

            if(canMove)
            {
                parryIsOn = false;
                anim.applyRootMotion = false;

                //Debug
                timer += Time.deltaTime;
                if(timer > 3)
                {
                    DoAction();
                    timer = 0;
                }
            }
            characterStats.poise -= delta * poiseDegrade;
            if (characterStats.poise < 0)
                characterStats.poise = 0;

        }

        void DoAction()
        {
            anim.Play("oh_attack_1");
            anim.applyRootMotion = true;
            anim.SetBool("can_move", false);
        }

        public void DoDamage(Action a)
        {
            if (isInvincible)
                return;

            int damage = StatsCalculations.CalculateBaseDamage(a.weaponStats, characterStats);

            characterStats.poise += damage;
            health -= damage;

            if (canMove || characterStats.poise > 100)
            {
                if (a.overrideDamageAnim)
                    anim.Play(a.damageAnim);
                else
                {
                    int ran = Random.Range(0, 100);
                    string tA = (ran > 50) ? StaticStrings.damage_1 : StaticStrings.damage_2;
                    anim.Play(tA);
                }
            }

            if(floatingTextPrefab)
                StartCoroutine(ShowFloatingText(damage, 0));

            Debug.Log("Damage is " + damage + " Poise is " + characterStats.poise);

            isInvincible = true;


            anim.applyRootMotion = true;
            anim.SetBool("can_move", false);
        }

        IEnumerator ShowFloatingText(int damage, int time)
        {
            yield return new WaitForSeconds(time);
            var go = Instantiate(floatingTextPrefab, transform.position, Quaternion.identity, transform);
            go.GetComponent<TextMeshPro>().text = damage.ToString();
            go.transform.LookAt(Camera.main.transform);
            go.transform.Rotate(0, 180, 0);
        }

        public void CheckForParry(Transform target, StateManager states)
        {
            if (parryIsOn == false || canBeParried == false || isInvincible)
                return;

            Vector3 dir = transform.position - target.position;
            dir.Normalize();
            float dot = Vector3.Dot(target.forward, dir);
            if (dot < 0)
                return;

            isInvincible = true;
            anim.Play("attack_interrupt");
            anim.applyRootMotion = true;
            anim.SetBool("can_move", false);
            //states.parryTarget = this;
            parriedBy = states;
            return;
        }

        public void IsGettingParried(WeaponStats weaponStats)
        {
            int damage = StatsCalculations.CalculateBaseDamage(weaponStats, characterStats);
            health -= damage;
            dontDoAnything = true;
            anim.SetBool(StaticStrings.can_move, false);
            anim.Play(StaticStrings.parry_received);
            if (floatingTextPrefab)
                StartCoroutine(ShowFloatingText(damage, 2));
        }

        public void IsGettingBackstabbed(WeaponStats weaponStats)
        {
            int damage = StatsCalculations.CalculateBaseDamage(weaponStats, characterStats);
            health -= damage;
            dontDoAnything = true;
            anim.SetBool(StaticStrings.can_move, false);
            anim.Play(StaticStrings.backstabbed);
            if (floatingTextPrefab)
                StartCoroutine(ShowFloatingText(damage, 2));
        }
    }
}

