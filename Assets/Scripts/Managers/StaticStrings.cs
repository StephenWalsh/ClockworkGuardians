﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public static class StaticStrings
    {
        // Inputs
        public static string Vertical = "Vertical";
        public static string Horizontal = "Horizontal";
        public static string B = "B";
        public static string A = "A";
        public static string Y = "Y";
        public static string X = "X";
        public static string RT = "RT";
        public static string LT = "LT";
        public static string RB = "RB";
        public static string LB = "LB";
        public static string L = "L";

        // Animator Parameters
        public static string vertical = "vertical";
        public static string horizontal = "horizontal";
        public static string can_move = "can_move";
        public static string interacting = "interacting";
        public static string lockon = "lockon";
        public static string on_ground = "on_ground";
        public static string run = "run";
        public static string mirror = "mirror";
        public static string blocking = "blocking";
        public static string isLeft = "isLeft";
        public static string animSpeed = "animSpeed";
        public static string two_handed = "two_handed";
        public static string parry_attack = "parry_attack";

        // Animator States
        public static string Rolls = "Rolls";
        public static string attack_interrupt = "attack_interrupt";
        public static string parry_received = "parry_received";
        public static string backstabbed = "backstabbed";
        public static string damage_1 = "damage_1";
        public static string damage_2 = "damage_2";
        public static string damage_3 = "damage_3";


    }
}

